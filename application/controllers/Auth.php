<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function login()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		
		if($method != METHOD_POST){
			json_output(STATUS_BAD_REQUEST,array(RESPONSE_STATUS => STATUS_BAD_REQUEST,RESPONSE_MESSAGE => 'Bad request.'));
		} else {

			$check_auth_client = $this->PerifitModel->check_auth_client();
			
			if($check_auth_client == TRUE){
				
				//$params = $this->input->raw_input_stream;
				$rawParams = $this->input->raw_input_stream;
		
		       	$params = json_decode($rawParams, TRUE);

		        $username = $params['username'];
		        $password = $params['password'];

				$response = $this->PerifitModel->login($username,$password);
				$statusResp = STATUS_OK;
				
				switch ($response[RESPONSE_ERROR])
				{
					case ERROR_INVALID_ACCOUNT:
						$statusResp = STATUS_UNAUTHORIZED;
						break;

					case ERROR_INTERNAL:
						$statusResp = STATUS_INTERNAL_ERROR;
						break;

					case ERROR_NONE:
						$statusResp = STATUS_OK;
						break;
				}

				json_output($statusResp, $response);
			}
		}
		
	}

	public function logout()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != METHOD_POST){
			json_output(STATUS_BAD_REQUEST, array(RESPONSE_ERROR => ERROR_BAD_REQUEST, RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();
			if($check_auth_client == TRUE){
		        $response = $this->PerifitModel->logout();
				json_output(STATUS_OK, $response);
			}
		}
	}
	
}
