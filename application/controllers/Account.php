<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model('AccountModel');
    }

	public function index()
	{
		json_output(STATUS_BAD_REQUEST, array(RESPONSE_ERROR => ERROR_BAD_REQUEST, RESPONSE_MESSAGE => 'Bad request.'));	
	}

	public function create()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != METHOD_POST ){
			json_output(STATUS_BAD_REQUEST,array(RESPONSE_ERROR => ERROR_BAD_REQUEST, RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();

			if($check_auth_client == true){

				$params = json_decode(file_get_contents('php://input'), TRUE);
				$email = $params['username'];
				$name = $params['name'];
				$password = $params['password'];

				$respStatus = STATUS_OK;

				if ($email == "" || $name == "" || $password == "" ) {
					$respStatus = STATUS_BAD_REQUEST;
					$resp = array(	RESPONSE_ERROR => ERROR_MISS_INFO, 
									RESPONSE_MESSAGE =>  'Email, name & password can\'t empty');
				} else {
					// valid name
					if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
						$respStatus = STATUS_BAD_REQUEST;
						$resp = array(RESPONSE_ERROR => ERROR_INVALID_NAME, RESPONSE_MESSAGE =>  'The name only allow letters and white space');
					}
					// valid email
					else if ($this->AccountModel->is_exist($email) == TRUE) {
						$respStatus = STATUS_BAD_REQUEST;
						$resp = array(RESPONSE_ERROR => ERROR_INVALID_EMAIL, RESPONSE_MESSAGE =>  'The email already exists in the system');
						
					} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
						$respStatus = STATUS_BAD_REQUEST;
						$resp = array(RESPONSE_ERROR => ERROR_INVALID_EMAIL, RESPONSE_MESSAGE =>  'Invalid email format');
					}
					// valid password
					else if (strlen($password) < 6) {
						$respStatus = STATUS_BAD_REQUEST;
						$resp = array(RESPONSE_ERROR => ERROR_INVALID_PASSWORD, RESPONSE_MESSAGE =>  'Password must be at least 6 characters');
					} else {
						$params['password'] = md5($params['password']);
						$resp = $this->AccountModel->create($params);
					}
				}
				json_output($respStatus, $resp);
			}
		}
	}
}
