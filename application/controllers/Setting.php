<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model('UsersSettingsModel');
    }

	public function index()
	{
		json_output(STATUS_BAD_REQUEST, array(RESPONSE_ERROR => ERROR_BAD_REQUEST, RESPONSE_MESSAGE => 'Bad request.'));	
	}

	public function detail($user_id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != METHOD_GET || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE) {
			json_output(STATUS_BAD_REQUEST, array(RESPONSE_ERROR => ERROR_BAD_REQUEST, RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();

			if($check_auth_client == true){ 

				$respStatus = STATUS_OK;

				$resp = $this->UsersSettingsModel->detail_by_user_id($user_id);

				json_output($respStatus, $resp);
			}
		}
	}

	public function update($field, $user_id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != METHOD_PUT || $this->uri->segment(3) == '' || $this->uri->segment(4) == '' || is_numeric($this->uri->segment(4)) == FALSE){
			json_output(STATUS_BAD_REQUEST, array(RESPONSE_STATUS => STATUS_BAD_REQUEST,RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();
			if($check_auth_client == true){
				$response = $this->PerifitModel->auth();
				$respStatus = STATUS_OK;
			
		        if($response[RESPONSE_ERROR] == ERROR_NONE) {
					$params = $this->input->raw_input_stream;

					if ($params == "") {
						$respStatus = STATUS_BAD_REQUEST;
						$response = array(RESPONSE_ERROR => ERROR_BAD_REQUEST, RESPONSE_MESSAGE =>  'Data can\'t empty');
					} else {
						switch ($field)
						{
							case 'game':
								$response = $this->UsersSettingsModel->update_game($user_id, $params);
							break;

							case 'weekly_gold':
								$response = $this->UsersSettingsModel->update_weekly_gold($user_id, $params);
							break;

							case 'notification':
								$response = $this->UsersSettingsModel->update_notification($user_id, $params);
							break;

							case 'theme':
								$response = $this->UsersSettingsModel->update_theme($user_id, $params);
							break;
						}
					}
				}
				
				json_output($respStatus, $response);
			}
		}
	}

	public function update_weely_gold($user_id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != METHOD_PUT || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(STATUS_BAD_REQUEST, array(RESPONSE_STATUS => STATUS_BAD_REQUEST,RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();
			if($check_auth_client == true){
				$response = $this->PerifitModel->auth();
				$respStatus = STATUS_OK;
			
		        if($response[RESPONSE_ERROR] == ERROR_NONE) {
					$params = $this->input->raw_input_stream;

					if ($params == "") {
						$respStatus = STATUS_BAD_REQUEST;
						$response = array(RESPONSE_ERROR => ERROR_BAD_REQUEST, RESPONSE_MESSAGE =>  'Game setting can\'t empty');
					} else {
		        		$response = $this->UsersSettingsModel->update_game($user_id, $params);
					}
				}
				
				json_output($respStatus, $response);
			}
		}
	}
}
