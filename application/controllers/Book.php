<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->PerifitModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

	public function index()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != METHOD_GET){
			json_output(400,array(RESPONSE_STATUS => STATUS_BAD_REQUEST, RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->PerifitModel->auth();
		        if($response[RESPONSE_STATUS] == STATUS_OK){
		        	$resp = $this->PerifitModel->book_all_data();
	    			json_output($response[RESPONSE_STATUS],$resp);
		        }
			}
		}
	}

	public function detail($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != METHOD_GET || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array(RESPONSE_STATUS => 400,RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->PerifitModel->auth();
		        if($response[RESPONSE_STATUS] == STATUS_OK){
		        	$resp = $this->PerifitModel->book_detail_data($id);
					json_output($response[RESPONSE_STATUS],$resp);
		        }
			}
		}
	}

	public function create()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != METHOD_POST ){
			json_output(400,array(RESPONSE_STATUS => 400,RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->PerifitModel->auth();
		        $respStatus = $response[RESPONSE_STATUS];
		        if($response[RESPONSE_STATUS] == STATUS_OK){
					$params = json_decode(file_get_contents('php://input'), TRUE);
					if ($params['title'] == "" || $params['author'] == "") {
						$respStatus = 400;
						$resp = array(RESPONSE_STATUS => 400,RESPONSE_MESSAGE =>  'Title & Author can\'t empty');
					} else {
		        		$resp = $this->PerifitModel->book_create_data($params);
					}
					json_output($respStatus,$resp);
		        }
			}
		}
	}

	public function update($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != METHOD_PUT || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array(RESPONSE_STATUS => 400,RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->PerifitModel->auth();
		        $respStatus = $response[RESPONSE_STATUS];
		        if($response[RESPONSE_STATUS] == STATUS_OK){
					$params = json_decode(file_get_contents('php://input'), TRUE);
					$params['updated_at'] = date('Y-m-d H:i:s');
					if ($params['title'] == "" || $params['author'] == "") {
						$respStatus = 400;
						$resp = array(RESPONSE_STATUS => 400,RESPONSE_MESSAGE =>  'Title & Author can\'t empty');
					} else {
		        		$resp = $this->PerifitModel->book_update_data($id,$params);
					}
					json_output($respStatus,$resp);
		        }
			}
		}
	}

	public function delete($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'DELETE' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array(RESPONSE_STATUS => STATUS_BAD_REQUEST, RESPONSE_MESSAGE => 'Bad request.'));
		} else {
			$check_auth_client = $this->PerifitModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->PerifitModel->auth();
		        if($response[RESPONSE_STATUS] == STATUS_OK){
		        	$resp = $this->PerifitModel->book_delete_data($id);
					json_output($response[RESPONSE_STATUS],$resp);
		        }
			}
		}
	}

}
