<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class UsersSettingsModel extends CI_Model {
    var $tbl_main = "users_settings";

    public function detail_by_user_id($id)
    {
        return $this->db->select('*')
                        ->from($this->tbl_main)
                        ->where('user_id',$id)
                        //->order_by('id','desc')
                        ->get()
                        ->row();
    }

    public function create_default($userId)
    {
        $data = array (
            'user_id' => $userId,
            'game' =>  '[{"game":0, "theme":1, "selected":1},{"game":1, "theme":1, "selected":0}]',
            'weekly_gold' => '30',
            'notification' => '0'
        );

        $this->db->insert($this->tbl_main, $data);
        return array(RESPONSE_ERROR => ERROR_NONE, RESPONSE_MESSAGE => 'Data has been created.');
    }

    public function update_game($user_id, $game)
    {
        $data = array (
            "game" => $game
        );
        //echo $game;
        $this->db->where('user_id', $user_id)
                 ->update($this->tbl_main, $data);

         return array(RESPONSE_ERROR => ERROR_NONE, RESPONSE_MESSAGE => 'Data has been updated.');
    }

    public function update_weekly_gold($user_id, $weekly_gold)
    {
        $data = array (
            "weekly_gold" => $weekly_gold
        );
        $this->db->where('user_id', $user_id)
                 ->update($this->tbl_main, $data);

         return array(RESPONSE_ERROR => ERROR_NONE, RESPONSE_MESSAGE => 'Data has been updated.');
    }

    public function update_notification($user_id, $notification)
    {
        $data = array (
            "notification" => $notification
        );
        $this->db->where('user_id', $user_id)
                 ->update($this->tbl_main, $data);

         return array(RESPONSE_ERROR => ERROR_NONE, RESPONSE_MESSAGE => 'Data has been updated.');
    }

    public function update_theme($user_id, $theme)
    {
        $data = array (
            "theme" => $theme
        );
        //echo $game;
        $this->db->where('user_id', $user_id)
                 ->update($this->tbl_main, $data);

         return array(RESPONSE_ERROR => ERROR_NONE, RESPONSE_MESSAGE => 'Data has been updated.');
    }

    // public function book_delete_data($id)
    // {
    //     $this->db->where('id',$id)->delete('books');
    //     return array(RESPONSE_STATUS => STATUS_OK,RESPONSE_MESSAGE => 'Data has been deleted.');
    // }
}
