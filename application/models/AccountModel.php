<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class AccountModel extends CI_Model {

    var $tbl_main = "users";

    public function create($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->tbl_main, $data);
        $userId = $this->db->insert_id();
        $this->load->model('UsersSettingsModel');
        $this->UsersSettingsModel->create_default($userId);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array(RESPONSE_ERROR => ERROR_INTERNAL, RESPONSE_MESSAGE => 'Internal server error.');
        } else {
            $this->db->trans_commit();
            return array(RESPONSE_ERROR => ERROR_NONE, RESPONSE_MESSAGE => 'User has been created.');
        }
    }
  
    function is_exist($email)
	{
		$this->db->select('id');		    
		$this->db->from($this->tbl_main);
		$this->db->where('username', $email);
		
		$query = $this->db->get();
		
		return ($query->num_rows() > 0);
	}
}
