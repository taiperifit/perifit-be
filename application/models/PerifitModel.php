<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class PerifitModel extends CI_Model {

    var $client_service = CLIENT_SERVICE;
    var $auth_key       = AUTH_KEY;

    public function check_auth_client(){
        $client_service = $this->input->get_request_header(HEADER_CLIENT_SERVICE, TRUE);
        $auth_key  = $this->input->get_request_header(HEADER_AUTH_KEY, TRUE);

        if($client_service == $this->client_service && $auth_key == $this->auth_key){
            return true;
        } else {
            return json_output(STATUS_UNAUTHORIZED, array(RESPONSE_ERROR => ERROR_UNAUTHORIZED, RESPONSE_MESSAGE => 'Unauthorized.'));
        }
    }

    public function login($username,$password)
    {
        $q  = $this->db->select('password, id')->from('users')->where('username',$username)->where('password', $password)->get()->row();
       
        if($q == "") {
            return array(RESPONSE_ERROR => ERROR_INVALID_ACCOUNT, RESPONSE_MESSAGE => 'Incorrect username or password');
        } else {
            $id = $q->id;
            $last_login = date('Y-m-d H:i:s');
            $token = crypt(substr( md5(rand()), 0, 7));
            $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
            $this->db->trans_start();
            $this->db->where('id',$id)->update('users',array('last_login' => $last_login));
            $this->db->insert('users_authentication',array('users_id' => $id,'token' => $token,'expired_at' => $expired_at));
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array(RESPONSE_ERROR => ERROR_INTERNAL, RESPONSE_MESSAGE => 'Internal server error.');
            } else {
                $this->db->trans_commit();
                return array(RESPONSE_ERROR => ERROR_NONE, RESPONSE_MESSAGE => 'Successfully login.','id' => $id, 'token' => $token);
            }
        }
    }

    public function logout()
    {
        $users_id  = $this->input->get_request_header(HEADER_USER_ID, TRUE);
        $token     = $this->input->get_request_header(HEADER_AUTHORIZATION, TRUE);
        $this->db->where('users_id',$users_id)->where('token',$token)->delete('users_authentication');
        return array(RESPONSE_ERROR => ERROR_NONE, RESPONSE_MESSAGE => 'Successfully logout.');
    }

    public function auth()
    {
        $users_id  = $this->input->get_request_header(HEADER_USER_ID, TRUE);
        $token     = $this->input->get_request_header(HEADER_AUTHORIZATION, TRUE);
        $q         = $this->db->select('expired_at')->from('users_authentication')->where('users_id',$users_id)->where('token',$token)->get()->row();

        if ($q == ""){
            return array(RESPONSE_ERROR => ERROR_UNAUTHORIZED, RESPONSE_MESSAGE => 'Unauthorized.');
        } else {
            if($q->expired_at < date('Y-m-d H:i:s')){
                return array(RESPONSE_ERROR => ERROR__UNAUTHORIZED, RESPONSE_MESSAGE => 'Your session has been expired.');
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('users_id',$users_id)->where('token',$token)->update('users_authentication',array('expired_at' => $expired_at,'updated_at' => $updated_at));
                return array(RESPONSE_ERROR => ERROR_NONE, RESPONSE_MESSAGE => 'Authorized.');
            }
        }
    }

    // public function book_all_data()
    // {
    //     return $this->db->select('id,title,author')->from('books')->order_by('id','desc')->get()->result();
    // }

    // public function book_detail_data($id)
    // {
    //     return $this->db->select('id,title,author')->from('books')->where('id',$id)->order_by('id','desc')->get()->row();
    // }

    // public function book_create_data($data)
    // {
    //     $this->db->insert('books',$data);
    //     return array(RESPONSE_STATUS => STATUS_CREATED, RESPONSE_MESSAGE => 'Data has been created.');
    // }

    // public function book_update_data($id,$data)
    // {
    //     $this->db->where('id',$id)->update('books',$data);
    //     return array(RESPONSE_STATUS => STATUS_OK,RESPONSE_MESSAGE => 'Data has been updated.');
    // }

    // public function book_delete_data($id)
    // {
    //     $this->db->where('id',$id)->delete('books');
    //     return array(RESPONSE_STATUS => STATUS_OK,RESPONSE_MESSAGE => 'Data has been deleted.');
    // }

}
